CREATE TABLE Contact (
    ID int NOT NULL IDENTITY,
    Firstname VARCHAR(255) NOT NULL,
    Lastname VARCHAR(255) NOT NULL,
    Address VARCHAR(255) NOT NULL,
    Status VARCHAR(255) NOT NULL,
    VatNumber VARCHAR(255),
    PRIMARY KEY (ID)
);

CREATE TABLE Company  (
    ID int NOT NULL IDENTITY,
    Address VARCHAR(255) NOT NULL,
    VatNumber VARCHAR(255) NOT NULL,
    ContactID int,
    PRIMARY KEY (ID),
    FOREIGN KEY (ContactID) REFERENCES Contact(ID)
);