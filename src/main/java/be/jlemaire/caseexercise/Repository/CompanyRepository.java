package be.jlemaire.caseexercise.Repository;

import be.jlemaire.caseexercise.Entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query("SELECT c FROM Company c WHERE LOWER(c.vatNumber) LIKE CONCAT('%', ?1, '%')")
    List<Company> findByVatNumber(String vatNumber);
}
