package be.jlemaire.caseexercise;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("be.jlemaire.caseexercise.Repository")
public class AppConfig {
}
