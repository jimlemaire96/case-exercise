package be.jlemaire.caseexercise.Service;

import be.jlemaire.caseexercise.Entity.Contact;
import be.jlemaire.caseexercise.Enum.ContactStatus;
import be.jlemaire.caseexercise.Exception.ValidationException;

public class ValidationService {
    public static boolean validateContactPayload(Contact contact) throws ValidationException{
        if(contact.getStatus().equals(ContactStatus.FREELANCE) &&
                (contact.getVatNumber() == null || contact.getVatNumber().isEmpty() || contact.getVatNumber().trim().isEmpty())
        ){
            throw new ValidationException("Freelances must have a vatNumber.");
        }
        return true;
    }
}
