package be.jlemaire.caseexercise.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="COMPANY")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="Address", nullable=false, unique=false)
    private String address;

    @Column(name = "VatNumber", nullable = false, unique = false)
    private String vatNumber;


    @JsonBackReference
    @ManyToOne
    private Contact contact;

    public Company() {}

    public Company(String address, String vatNumber) {
        this.address = address;
        this.vatNumber = vatNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Objects.equals(id, company.id) && Objects.equals(address, company.address) && Objects.equals(vatNumber, company.vatNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, address, vatNumber);
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", vatNumber='" + vatNumber + '\'' +
                '}';
    }
}
