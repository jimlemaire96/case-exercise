package be.jlemaire.caseexercise.Entity;

import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import be.jlemaire.caseexercise.Enum.ContactStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="CONTACT")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="Firstname", nullable=false, unique=false)
    private String firstname;

    @Column(name="Lastname", nullable=false, unique=false)
    private String lastname;

    @Column(name="Address", nullable=false, unique=false)
    private String address;

    @Column(name = "Status", nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    private ContactStatus status;

    @Column(name = "VatNumber", nullable = true, unique = false)
    private String vatNumber;

    @JsonManagedReference
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "contact"
    )
    private List<Company> companies;

    public Contact() {}

    /**
     *
     * @param firstname
     * @param lastname
     * @param address
     * @param status
     * @param vatNumber
     */
    public Contact(String firstname, String lastname, String address, ContactStatus status, String vatNumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.status = status;
        this.vatNumber = vatNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
    this.address = address;
    }

    public ContactStatus getStatus() {
        return status;
    }

    public void setStatus(ContactStatus status) {
        this.status = status;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public void addCompany(Company company) {
        this.companies.add(company);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return id == contact.id && Objects.equals(firstname, contact.firstname) && Objects.equals(lastname, contact.lastname) && Objects.equals(address, contact.address) && status == contact.status && Objects.equals(vatNumber, contact.vatNumber) && Objects.equals(companies, contact.companies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, address, status, vatNumber, companies);
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", address='" + address + '\'' +
                ", status=" + status +
                ", vatNumber='" + vatNumber + '\'' +
                '}';
    }
}
