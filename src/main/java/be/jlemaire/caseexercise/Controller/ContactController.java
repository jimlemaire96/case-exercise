package be.jlemaire.caseexercise.Controller;

import be.jlemaire.caseexercise.Entity.Company;
import be.jlemaire.caseexercise.Entity.Contact;
import be.jlemaire.caseexercise.Exception.ValidationException;
import be.jlemaire.caseexercise.Repository.CompanyRepository;
import be.jlemaire.caseexercise.Repository.ContactRepository;
import be.jlemaire.caseexercise.Service.ValidationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
public class ContactController {

  private final ContactRepository contactRepo;
  private final CompanyRepository companyRepo;

  ContactController(ContactRepository contactRepo, CompanyRepository companyRepo) {
    this.contactRepo = contactRepo;
    this.companyRepo = companyRepo;
  }

  @GetMapping("/contacts")
  public ResponseEntity<List<Contact>> findAll() {
    try {
      List<Contact> contacts = new ArrayList<Contact>();
      contactRepo.findAll().forEach(contacts::add);

      if (contacts.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(contacts, HttpStatus.OK);
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/contacts")
  public ResponseEntity<Contact> createContact(@RequestBody Contact contact) {
    try {
      Contact contactToSave = new Contact(
              contact.getFirstname(),
              contact.getLastname(),
              contact.getAddress(),
              contact.getStatus(),
              contact.getVatNumber());
      ValidationService.validateContactPayload(contactToSave);
      Contact resultContact = contactRepo.save(contactToSave);
      return new ResponseEntity<>(resultContact, HttpStatus.CREATED);
    } catch (ValidationException e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @DeleteMapping("/contacts/{id}")
  public ResponseEntity<HttpStatus> deleteContact(@PathVariable("id") long id) {
    try {
      contactRepo.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/contacts/{id}")
  public ResponseEntity<Contact> updateContact(@PathVariable("id") long id, @RequestBody Contact contact) {
    try {
      ValidationService.validateContactPayload(contact);

      Optional<Contact> contactData = contactRepo.findById(id);

      if (contactData.isPresent()) {
        Contact contactToUpdate = contactData.get();
        contactToUpdate.setFirstname(contact.getFirstname());
        contactToUpdate.setLastname(contact.getLastname());
        contactToUpdate.setAddress(contact.getAddress());
        contactToUpdate.setStatus(contact.getStatus());
        contactToUpdate.setVatNumber(contact.getVatNumber());
        contactRepo.save(contactToUpdate);
        return new ResponseEntity<>(contactToUpdate, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    } catch (ValidationException e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/contacts/{contactId}/companies/{companyId}")
  public ResponseEntity<Contact> addContactToCompany(@PathVariable("contactId") long contactId, @PathVariable("companyId") long companyId){
    try {

      Optional<Company> companyData = companyRepo.findById(companyId);
      Optional<Contact> contactData = contactRepo.findById(contactId);

      if (companyData.isPresent() && contactData.isPresent()) {
        Company companyToUpdate = companyData.get();
        Contact contactToAdd = contactData.get();
        companyToUpdate.setContact(contactToAdd);
        companyRepo.save(companyToUpdate);
        return new ResponseEntity<>(contactToAdd, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
