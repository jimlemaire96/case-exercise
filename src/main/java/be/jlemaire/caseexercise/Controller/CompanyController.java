package be.jlemaire.caseexercise.Controller;

import be.jlemaire.caseexercise.Entity.Company;
import be.jlemaire.caseexercise.Repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class CompanyController {

  private final CompanyRepository companyRepo;

  CompanyController(CompanyRepository companyRepo) {
    this.companyRepo = companyRepo;
  }

  @GetMapping("/companies")
  public ResponseEntity<List<Company>> findAll(@RequestParam(name="vatNumber", required = false) String vatNumberSearch) {
    try {
      List<Company> companies = new ArrayList<Company>();
      if(vatNumberSearch != null) companyRepo.findByVatNumber(vatNumberSearch.toLowerCase()).forEach(companies::add);
      else companyRepo.findAll().forEach(companies::add);

      if (companies.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(companies, HttpStatus.OK);
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/companies")
  public ResponseEntity<Company> createCompany(@RequestBody Company company) {
    try {
      Company companyToSave = new Company(
              company.getAddress(),
              company.getVatNumber());
      Company resultCompany = companyRepo.save(companyToSave);
      return new ResponseEntity<>(resultCompany, HttpStatus.CREATED);
    }  catch (Exception e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/companies/{id}")
  public ResponseEntity<Company> updateCompany(@PathVariable("id") long id, @RequestBody Company company) {
    try {

      Optional<Company> companyData = companyRepo.findById(id);

      if (companyData.isPresent()) {
        Company companyToUpdate = companyData.get();
        companyToUpdate.setAddress(company.getAddress());
        companyToUpdate.setVatNumber(company.getVatNumber());
        companyRepo.save(companyToUpdate);
        return new ResponseEntity<>(companyToUpdate, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
